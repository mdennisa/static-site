import React, { Component } from 'react';

class Contact extends Component 
{
    render() 
    {
        return (
            <div className="container">
                <div className="row">
                    <div className="col s12">
                        <h1>Contact</h1>
                        <p>My name is: Dennis</p>
                        <p>Phone: 017 651 1594</p>
                    </div>
                </div>
            </div>
        )
    }
}

export default Contact;