import React, { Component } from 'react';
import firebase from '../config.js';

class Patient extends Component
{
    _isMounted = false

    constructor()
    {
        super()
        this.state = {
            _showLoader: true,
            patients: []
        }

        this.db = firebase.firestore()
    }

    componentDidMount()
    {
        this._isMounted = true
        const list = []

        this.db.collection("Patients")
            .orderBy("name", "asc")
            .get()
            .then(querySnapshot => {
                if (this._isMounted) {
                    querySnapshot.forEach(doc => {
                        list.push({
                            id: doc.id,
                            name: doc.data().name
                        })
                    })
    
                    this.setState({
                        _showLoader: false,
                        patients: list
                    })
                }
            })
    }     
    
    componentWillUnmount()
    {
        this._isMounted = false
        const unsub = this.db.collection("Patients").onSnapshot(() => {})
        unsub()
    }

    render()
    {
        const { patients, _showLoader } = this.state
        return (
            <div className="container">
                <div className="row">
                    <div className="col s12">
                        <h4>Patient List</h4>
                        { _showLoader ? 
                            <div>Loading ...</div> : ''
                        }
                        <ul className="browser-default">
                            {patients.map(p => (
                                <li key={p.id}>{p.name}</li>
                            ))}
                        </ul>
                    </div>
                </div>   
            </div>
        )
    }
}

export default Patient;