import React, { Component } from 'react';

class Home extends Component
{
    render() 
    {
        return (
            <div className="container">
                <div className="row">
                    <div className="col s12">
                        <h1>React static page</h1>
                        <p>Welcome to my page</p>
                    </div>
                </div>
            </div>
        )
    }
}

export default Home;