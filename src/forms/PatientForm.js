import React, { Component } from 'react'
import firebase from '../config.js';

// import { withRouter } from 'react-router-dom'

export class PatientForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            patient_name: ''
        }
    }

    handleChange = e => {
        const { value } = e.target
        this.setState({
            patient_name: value
        })
    }

    handleSubmit = e => {
        e.preventDefault();
        const db = firebase.firestore()
        
        db.collection("Patients").add({
            name: this.state.patient_name
        }).then(doc => {
            this.setState({
                patient_name: ''
            })
            // redirect
            this.props.history.push('/patient')
        }).catch(error => {
            console.error(error)
        })

        
    }

    render() {
        const { patient_name } = this.state

        return (
            <div className="container">
                <div className="row">
                    <div className="col s-12">

                        <h4>Add Patient</h4>

                        <form onSubmit={this.handleSubmit}>
                        <div className="row">
                            <div className="input-field col s-6">
                                <input 
                                    type="text"
                                    id="patient_name"
                                    name="patient_name" 
                                    value={patient_name}
                                    onChange={this.handleChange} 
                                />
                                <label htmlFor="patient_name">Patient Name</label>
                            </div>
                            <div className="input-field col s-6">
                                <button className="btn waves-effect">Submit</button>
                            </div>
                        </div>
                        </form>

                    </div>
                </div>
            </div>
        )
    }
}

export default PatientForm
// export default withRouter(PatientForm)
