import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch, Link } from 'react-router-dom';

import 'materialize-css/dist/css/materialize.min.css';
import 'materialize-css/dist/js/materialize.min.js';

import Home from './pages/Home';
import About from './pages/About';
import Contact from './pages/Contact';

import Patient from './pages/Patient';
import PatientForm from './forms/PatientForm';

class App extends Component {

  render() {
    
    return (
      <Router>
        
        <nav>
          <div className="nav-wrapper container">
              <a href="/" className="brand-logo">Static Page</a>
              <ul id="nav-mobile" className="right hide-on-med-and-down">
                <CustomLi label="Home" to="/" exact={true} />
                <CustomLi label="About" to="/about" />
                <CustomLi label="Contact" to="/contact" />
                <CustomLi label="Patient" to="/patient" />
                <CustomLi label="Add Patient" to="/patientform" />
              </ul>
          </div>
        </nav>
        
        <Switch>
          <Route path="/" exact component={Home} />
          <Route path="/about" component={About} />
          <Route path="/contact" component={Contact} />
          <Route path="/patient" component={Patient} />
          <Route path="/patientform" component={PatientForm} />
        </Switch>
      </Router>
    );
  }
}

const CustomLi = ({ label, to, exact }) => {
  return (
    <Route
      path={to}
      exact={exact}
      children={({ match }) =>
        (
          <li className={ match ? "active" : "" }>
            <Link to={to}>{label}</Link>
          </li>
        )
      }
    />
  )
}

export default App;
